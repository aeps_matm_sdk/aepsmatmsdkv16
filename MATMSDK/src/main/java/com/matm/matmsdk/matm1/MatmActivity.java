package com.matm.matmsdk.matm1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.matm.matmsdk.Utils.MATMSDKConstant;
import com.matm.matmsdk.aepsmodule.utils.AepsSdkConstants;
import com.matm.matmsdk.aepsmodule.utils.Session;
import org.json.JSONException;
import org.json.JSONObject;
import isumatm.androidsdk.equitas.R;

public class MatmActivity extends AppCompatActivity implements View.OnClickListener,MicroAtmContract.View {

    Session session;
    MicroAtmPresenter microAtmPresenter;
    ProgressDialog pd;
    String encData;
    String authentication;
    MicroAtmTransactionModel microAtmTransactionModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matm);
        session = new Session(MatmActivity.this);
        microAtmPresenter = new MicroAtmPresenter(this);
        pd = new ProgressDialog(MatmActivity.this);



        if(MATMSDKConstant.applicationType.equalsIgnoreCase("CORE")){
            session.setUserToken(AepsSdkConstants.tokenFromCoreApp);
            session.setUsername(AepsSdkConstants.userNameFromCoreApp);

        }else {
            if (MATMSDKConstant.encryptedData.trim().length() != 0) {

                getUserAuthToken();
            } else {
                showAlert("Request parameters are missing. Please check and try again..");
            }
        }
    }



    private void getUserAuthToken(){
        String url = AepsSdkConstants.BASE_URL+"api/getAuthenticateData" ;
        JSONObject obj = new JSONObject();
        try {
            obj.put("encryptedData",MATMSDKConstant.encryptedData);
            obj.put("retailerUserName",MATMSDKConstant.loginID);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");

                                if(status.equalsIgnoreCase("success")) {
                                    String userName = obj.getString("username");
                                    String userToken = obj.getString("usertoken");
                                    session.setUsername(userName);
                                    session.setUserToken(userToken);
                                    //hideLoader();
                                    CallMatm1Api();

                                }else {
                                    showAlert(status);
                                    hideLoader();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                showAlert("Invalid Encrypted Data");
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            hideLoader();

                        }

                    });
        }catch ( Exception e){
            e.printStackTrace();
        }
    }

    private void CallMatm1Api() {

      /*  showLoader();
        if (Utils.isMicroATMDevicePaired(MatmActivity.this) == true) {

           if(MATMSDKConstant.transactionType.equalsIgnoreCase(MATMSDKConstant.balanceEnquiry)){
                balanceEnquiryApiCalling();
            }else{
                apiCalling();
            }
        } else {
            hideLoader();
            showAlert("Connect Bluetooth device.");
        }*/
    }

    public void showAlert(String msg){

        AlertDialog.Builder builder = new AlertDialog.Builder(MatmActivity.this);
        builder.setTitle("Alert!!");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void checkRequestCode(String status, String message, MicroAtmResponse microAtmResponse) {

        if(status != null && !status.matches("")) {
            String trans_type = "";
            if (MATMSDKConstant.transactionType.equalsIgnoreCase(MATMSDKConstant.cashWithdrawal)) {
                trans_type = "cash";
            }else{
                trans_type = "balance";
            }

            Intent intent = new Intent(Intent.ACTION_DATE_CHANGED);
            PackageManager manager = getPackageManager();
            intent = manager.getLaunchIntentForPackage("com.matm.matmservice");
            intent.putExtra("RequestData", encData);
            intent.putExtra("HeaderData", authentication);
            intent.putExtra("ReturnTime", 5);
            intent.putExtra("IS_PAIR_DEVICE",false);
            intent.putExtra("Flag","transaction");
            intent.putExtra("TransactionType",trans_type);
            intent.putExtra("client_id","");
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            startActivityForResult(intent, 1);
        }else{
            showAlert(message);
        }


    }

 /*   @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null & resultCode == RESULT_OK) {
            microAtmTransactionModel = new MicroAtmTransactionModel();
            if (requestCode == 1) {
                String response;
                if (data.hasExtra("ClientResponse")) {
                    hideLoader();
                    response = data.getStringExtra("ClientResponse");
                    if (!response.equalsIgnoreCase("")) {
                        try {
                            String strDecryptResponse = AES_BC.getInstance().decryptDecode(Utils.replaceNewLine(response), MatmConstant.CLIENT_REQUEST_ENCRYPTION_KEY);
                            Log.v("radha", "respones transaction : " + strDecryptResponse);
                            if (MATMSDKConstant.transactionType.equalsIgnoreCase(MATMSDKConstant.cashWithdrawal)) {
                               // if (rbCashWithdrawal.isChecked()) {
                                JSONObject jsonObject = new JSONObject(strDecryptResponse);
                                microAtmTransactionModel.setTxnStatus(jsonObject.getString("TxnStatus"));
                                microAtmTransactionModel.setTxnAmt(jsonObject.getString("TxnAmt"));
                                microAtmTransactionModel.setRnr(jsonObject.getString("RRN"));
                                microAtmTransactionModel.setCardNumber(jsonObject.getString("CardNumber"));
                                microAtmTransactionModel.setAvailableBalance(jsonObject.getString("AvailableBalance"));
                                microAtmTransactionModel.setTransactionDateandTime(jsonObject.getString("TransactionDatetime"));
                                microAtmTransactionModel.setTerminalId(jsonObject.getString("TerminalID"));
                                microAtmTransactionModel.setType("MATMcashWithdrawal");

                                Intent intentAtm = new Intent(MatmActivity.this,MATMTransactionStatusActivity.class);
                                intentAtm.putExtra(MatmConstant.MICRO_ATM_TRANSACTION_STATUS_KEY,microAtmTransactionModel);
                                startActivity(intentAtm);
                            }

                            else if (MATMSDKConstant.transactionType.equalsIgnoreCase(MATMSDKConstant.balanceEnquiry)) {
                                JSONObject jsonObject = new JSONObject(strDecryptResponse);
                                microAtmTransactionModel.setBalanceEnquiryStatus(jsonObject.getString("BalanceEnquiryStatus"));
                                microAtmTransactionModel.setRnr(jsonObject.getString("RRN"));
                                microAtmTransactionModel.setCardNumber(jsonObject.getString("CardNumber"));
                                microAtmTransactionModel.setAvailableBalance(jsonObject.getString("AvailableBalance"));
                                microAtmTransactionModel.setTransactionDateandTime(jsonObject.getString("TransactionDatetime"));
                                microAtmTransactionModel.setAccountNo(jsonObject.getString("AccountNo"));
                                microAtmTransactionModel.setTerminalId(jsonObject.getString("TerminalID"));
                                microAtmTransactionModel.setType("MATMbalanceEnquiry");
                            }

                        } catch (Exception e) {
                            microAtmTransactionModel = null;
                            Log.v("test", "error : " + e);
                        }
                    }
                } else if (data.hasExtra("ErrorDtls")) {
                    hideLoader();
                    response = data.getStringExtra("ErrorDtls");
                    if (!response.equalsIgnoreCase("")) {
                        try {
                            String[] error_dtls = response.split("\\|");
                            String errorMsg = error_dtls[0];
                            microAtmTransactionModel.setErrormsg(errorMsg);
                        } catch (Exception exp) {
                            microAtmTransactionModel = null;
                            Log.v("test", "Error : " + exp.toString());
                        }
                    }
                } else {
                    microAtmTransactionModel = null;
                }
            }
         *//*   if (requestCode == 3) {
                String response;
                if (data.hasExtra("DeviceConnectionDtls")) {
                    response = data.getStringExtra("DeviceConnectionDtls");
                    String[] error_dtls = response.split("\\|");
                    String errorMsg = error_dtls[0];
                    String errorMsg2 = error_dtls[1];
                    microAtmTransactionModel.setErrormsg(errorMsg2);
                    microAtmTransactionModel.setStatusError(errorMsg);
                    Log.v("radha","device response : "+response);

                }
            }*//*

        }
    }*/

    @Override
    public void checkEmptyFields() {

    }

    @Override
    public void showLoader() {
        if (pd ==null){
            pd = new ProgressDialog(MatmActivity.this);
            pd.setCancelable(false);
            pd.setMessage("Please Wait..");
        }
        pd.show();
    }

    @Override
    public void hideLoader() {
        if (pd!=null){
            pd.dismiss();
        }

    }

    public void apiCalling()
    {
        MicroAtmRequestModel microAtmRequestModel = new MicroAtmRequestModel(MATMSDKConstant.transactionAmount,"MATMcashWithdrawal","mobile");
        microAtmPresenter.performRequestData(session.getUserName(),session.getUserToken(), microAtmRequestModel);
    }

    public void balanceEnquiryApiCalling()
    {
        MicroAtmRequestModel microAtmRequestModel = new MicroAtmRequestModel("0","MATMbalanceEnquiry","mobile");
        microAtmPresenter.performRequestData(session.getUserName(),session.getUserToken(), microAtmRequestModel);

    }
}
